# 2) Comprendre le TP 1

## Objectif

Dans ce TP, nous essayons de créer l’infrastructure suivante :

![Untitled](2%20Comprendre%20le%20TP%201%20cc48c771ac9a4ac3b61af7d70477ce70/Untitled.png)

- Un réseau local et deux sous-réseaux
- Une machine “Nat_JumpHost” sur le sous-réseau qu’on va nommer “public subnet”
- Une machine “PrivateHost” sur le sous-réseau qu’on va nommer “private subnet”
- Nat_JumpHost doit avoir accès direct à internet (out). Et on doit pouvoir y accéder depuis notre ordinateur (in).
- PrivateHost doit avoir accès à internet (out). En revanche, il doit être isolé et seul une connexion depuis Nat_Jumphost est possible (in). Car on peut imaginer qu’il contient une infra critique (ex : BDD)

On configurerait aussi : 

![Untitled](2%20Comprendre%20le%20TP%201%20cc48c771ac9a4ac3b61af7d70477ce70/Untitled%201.png)

- Table de routage de l’ordi dans le réseau privé. ie. “PrivateHost” : la passerelle sera l’ordi public (il va nous servir de proxy).
- Son pare feu : bloquer tout sauf les connexions SSH entrantes depuis l’ordi public uniquement.

- Table de routage de l’ordi dans le réseau public. ie. “Nat_JumpHost” :
    - Tout le trafic sortant (requêtes…) doit être redirigé dans l’internet.
    - Les requêtes reçues du réseau privé doivent l’être aussi (IP forwarding).
- Son pare feu : seul mon ordi à l’efrei a le droit d’entrer.

![Untitled](2%20Comprendre%20le%20TP%201%20cc48c771ac9a4ac3b61af7d70477ce70/Untitled%202.png)

## Comment on traduit ça en langage amazon ?

- Un réseau local rien que pour moi : **VPC (Virtual Private Cloud)**.
- Une machine virtuelle : **EC2 (Elastic Compute Cloud)**.
- Routeur : Amazon abstrait ce concept grâce à des tables de routage (**route table**). On peut lui ajouter des règles (**route**). Ex : 0.0.0.0/16 → internet gateway qui veut dire “acheminer tous les paquets sur l’internet public”.
- Une passerelle pour aller à internet : **Internet Gateway**.
- Le pare feu d’une EC2 : par défaut, amazon bloque tout. On utilisera des **Security Groups (SG)** pour autoriser des paquets entrants (**inbound**) ou sortants (**outbound**).
    - Les security group sont pratiques car on peut les assigner à une ou plusieurs machines (le concept d’un groupe quoi.) et les référencer sur nos configurations de pare feu par exemple.
    

Amazon c’est des milliards de machines. Ils ont une zone géographique (**region**) possédant plusieurs entrepôts de datacenters. Un entrepôt est appelé **availability zone**.

Les clés SSH c’est parfois contraignant. Amazon nous propose des **IAM** (Identity and Access Management), un système permettant de nous identifier avec nos permissions et un système de profils. Grâce à l’**agent SSM** (SyStem Manager Agent), nous pouvons nous connecter aux machines sans copier ou stocker les clés. C’est pour ça que dans les “**user data**” on ajoutait `snap start amazon-ssm-agent` : grâce à ça nos scripts pourront interagir avec l’API Amazon sans devoir copier des clés, tant qu’on utilise le protocole d’Amazon en tout cas.

<aside>
⏩ Section suivante :

[Si j’ai de la motivation - TP 2 puis 3]

</aside>