# TP2 (incomplet)

## Préparation de l’environnement :

J’ai créé deux machines : ToolsCICD et RemoteServer

![Untitled](TP2%20incomplet%20955cea26d85f439b8f4f52a22d6f4e6a/Untitled.png)

J’ai installé ansible et packer depuis la doc officielle pour avoir la version la plus à jour

Structure des fichiers

.
└── BuildWebAmi
├── buildAMI.pkr.hcl
└── playbook
└── play.yml

```jsx
mkdir -p WebApp/BuildWebAmi/playbook

sudo apt update
sudo apt install software-properties-common

sudo add-apt-repository --yes --update ppa:ansible/ansible

curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"

wget -O- https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list

sudo apt install ansible
sudo apt install packer
sudo apt install terraform
```

```jsx
---
- name: Configuration du serveur web Apache pour AWS AMI
  hosts: all
  become: yes
  tasks:

    - name: Installation d'Apache
      apt:
        name: apache2
        state: latest
        update_cache: yes

    - name: Changement du port d'écoute pour Apache
      lineinfile:
        path: /etc/apache2/ports.conf
        regexp: '^Listen '
        line: Listen 8080
      notify: redémarrage apache

    - name: Configuration du VirtualHost pour écouter sur le port 8080
      copy:
        dest: /etc/apache2/sites-available/000-default.conf
        content: |
          <VirtualHost *:8080>
              ServerAdmin webmaster@localhost
              DocumentRoot /var/www/html
              ErrorLog ${APACHE_LOG_DIR}/error.log
              CustomLog ${APACHE_LOG_DIR}/access.log combined
          </VirtualHost>
      notify: redémarrage apache

    - name: Suppression du site web par défaut
      file:
        path: /var/www/html
        state: absent

    - name: Clonage du site web depuis GitHub
      git:
        repo: 'https://github.com/cloudacademy/static-website-example'
        dest: /var/www/html
        clone: yes
        update: yes

  handlers:
    - name: redémarrage apache
      service:
        name: apache2
        state: restarted
```

```jsx
packer {
    required_plugins {
        ansible = {
            version = "~> 1"
            source = "github.com/hashicorp/ansible"
        }
        amazon = {
            source = "github.com/hashicorp/amazon"
            version = "~> 1"
        }
    }
}
variable "associate_public_ip_address" {
    type = string
    default = "true"
}
variable "base_ami" {
    type = string
    default = "ami-0c7217cdde317cfec"
}
variable "instance_type" {
    type = string
    default = "t3.medium"
}
variable "region" {
    type = string
    default = "us-east-1"
}
variable "app_name" {
    type = string
    default = "WebApp"
}
variable "ssh_username" {
    type = string
    default = "ubuntu"
}
locals {
    timestamp = formatdate("DD_MM_YYYY-hh_mm", timestamp())
}
source "amazon-ebs" "static-web-ami" {
    ami_name = "${var.app_name}-${local.timestamp}"
    associate_public_ip_address = "${var.associate_public_ip_address}"
    instance_type = "${var.instance_type}"
    region = "${var.region}"
    source_ami = "${var.base_ami}"
    ssh_username = "${var.ssh_username}"
    iam_instance_profile = "LabInstanceProfile"
    tags = {
        Name = "${var.app_name}"
    }
}
build {
    sources = ["source.amazon-ebs.static-web-ami"]
    provisioner "ansible" {
        playbook_file = "./playbook/play.yml"
    }
}
```

ansible-playbook -i [ubuntu@3.231.31.153](mailto:ubuntu@3.231.31.153), --private-key ~/.ssh/logan.pem play.yml

packer init buildAMI.pkr.hcl

packer build buildAMI.pkr.hcl

(penser à ajouter le port 8080 dans le security group)

![Untitled](TP2%20incomplet%20955cea26d85f439b8f4f52a22d6f4e6a/Untitled%201.png)