# 1) Quelques définitions et rappels importants

## A) Le dev ops

<aside>
🔁 DevOps : culture visant à délivrer un projet au client plus rapidement, plus fiablement et avec plus de qualité.

</aside>

Pour cela, on fera appel à des outils tels que les infrastructures cloud, les intégrations/déploiements continus (CI/CD), des architectures microservices…

### 🧐 Infrastructure cloud ?

Pour déployer une (grosse) application, on a besoin d’un parc informatique. Des datacenters, une configuration réseau, des entrepôts, de la maintenance… Bref, ça peut coûter cher. **Solution :** utiliser des infrastructures cloud.

On va travailler tout au long des TPs avec Amazon Web Services, un leader dans le domaine.

Il est possible de louer des services tel que des machines virtuelles prêtes à emploi pour héberger nos applications, voire simplement des machines éphémères pour faire des tâches spécifiques. On les paie à l’usage.

### ✌ Intégration continue (CI) ?

Un développeur ça envoie du code sur git. Ce serait cool que dès que je push mon application web, une machine éphémère puisse packager mon code et exécuter les tests unitaires (plutôt que ce soit à moi de le faire manuellement).

![Untitled](1%20Quelques%20de%CC%81finitions%20et%20rappels%20importants%2046e3c615bda44716a5409b1f59d43cb8/Untitled.png)

### 📤 Déploiement continu (CD) ?

Quitte à packager l’application, autant qu’une machine éphémère s’occupe de la déployer sur un environnement (prod ou dev) à ma place. Et si quelque chose ne fonctionne pas, on retourne juste en arrière et un processus automatique va permettre de re-déployer l’ancienne version sans intervention humaine.

![Untitled](1%20Quelques%20de%CC%81finitions%20et%20rappels%20importants%2046e3c615bda44716a5409b1f59d43cb8/Untitled%201.png)

## B) Notions utiles

### 📶 Rappels en réseau

😶 Si vous êtes nul en réseau, voici quelques rappels :

Une entreprise peut louer quelques **adresses réseau**. Mais pour un gros parc informatique, elle peut avoir besoin de créer des **réseaux locaux**, séparés en **sous-réseaux**, connectés sur un **routeur**. Il faut configurer les machines pour avoir leur propre **adresse IP locale** (ex : 172.16.1.10), un **masque de sous réseau** (172.16.0.0/16) et **une passerelle** (gateway; ex 172.16.0.1 — souvent un routeur)

Quand des paquets IP transitent sur le réseau, on peut faire en sorte que le routeur puisse rediriger correctement les paquets, en utilisant une table de routage. Par exemple, prendre tous les paquets entrants et les acheminer sur l’internet ou sur un proxy.

### 👩‍💻 SSH

Pour ceux qui n’ont jamais fait de linux, ce terme peut vous paraître inconnu. SSH signifie Secure SHell, c’est un protocole permettant de se connecter à un ordinateur distant et de le contrôler via le shell.

SSH est disponible nativement sur mac, linux et Windows 10 (donc MobaXterm n’est pas vraiment nécessaire)

<aside>
👓 **Exemple :**

Je possède un serveur web ubuntu qui héberge ce site [https://default.kagescan.fr](https://default.kagescan.fr) 

“[default.kagescan.fr](http://default.kagescan.fr)” est un nom de domaine que j’ai loué et pointe en réalité vers l’IP publique de mon serveur 45.140.165.37 (vous pouvez le voir en faisant la commande `ping default.kagescan.fr` 🤓)

Mon nom d’utilisateur est logan. Donc pour me connecter au serveur, je ferai `ssh logan@default.kagescan.fr` et on me demandera mon MDP.

![Je peux taper des commandes qui s’exécuteront sur mon ordi distant !](1%20Quelques%20de%CC%81finitions%20et%20rappels%20importants%2046e3c615bda44716a5409b1f59d43cb8/Untitled%202.png)

Je peux taper des commandes qui s’exécuteront sur mon ordi distant !

</aside>

Sauf que un mot de passe c’est pas ouf car on peut le bruteforce. La solution : une clé SSH. Vous l’avez vu en cours de crypto, on aura une clé privée RSA pour s’identifier.

- Avantage : Impossible à bruteforce, ne transite pas dans le réseau, difficile à capturer sans accès à la machine.
- Inconvénient : vous la perdez, vous êtes foutus. Vu que la clé est dans un fichier, un pirate peut la voler via un virus.

L’usage d’une **clé privée type pem** est la méthode utilisée sur les machines amazon. AWS academy nous crée une clée privée téléchargeable nommée **vockey.pem**, ce qui ne nous empêche pas de créer nos propres clés si on le veut !

Pour se connecter en SSH à une machine avec une clé plutôt qu’un mot de passe, on ajoutera l’argument `-i <cheminDe/maClé.pem>`

Logiquement, si nous sommes sur une machine distante A et qu’on veut se connecter à une autre machine distante B, la clé privée de B doit être présente localement dans A.

### 📝 Les éditeurs de texte

Vous l’aurez compris, vous manipulerez vos machines uniquement avec une console, pas avec une interface graphique avec un joli bureau.

Vous avez quelques éditeurs de texte :

![`vim`, utilisé par le prof, réservé aux maso. Vous pouvez le tester pour le fun. *how do i exit vim ???*](1%20Quelques%20de%CC%81finitions%20et%20rappels%20importants%2046e3c615bda44716a5409b1f59d43cb8/Untitled%203.png)

`vim`, utilisé par le prof, réservé aux maso. Vous pouvez le tester pour le fun. *how do i exit vim ???*

![Untitled](1%20Quelques%20de%CC%81finitions%20et%20rappels%20importants%2046e3c615bda44716a5409b1f59d43cb8/Untitled%204.png)

`nano` est un peu plus intuitif.

---

J’ai ma petite préférence pour `micro`, mais le prof risque de vous crier dessus si vous avez des soucis avec, comme vous ne respectez pas son tp. À vos risques et périls.

Par contre il n’est pas pré-installé sur ubuntu. Donc il suffit de faire `sudo apt update` suivi de `sudo apt install micro` pour pouvoir l’utiliser.

Vous pouvez ensuite démarrer micro avec `micro nomDuFichierAModifierOuCreer.txt`.

Micro supporte la souris (scroll et multi curseur compris), plusieurs onglets, ajoute les numéros lignes, de la coloration syntaxique, un système de plugins et des raccourcis bien plus intuitifs (`Ctrl+S` pour sauvegarder, `Ctrl+Q` pour quitter)

![Untitled](1%20Quelques%20de%CC%81finitions%20et%20rappels%20importants%2046e3c615bda44716a5409b1f59d43cb8/Untitled%205.png)

Pour apprendre à utiliser micro, je vous recommande `Ctrl+E` suivi de `help`.

<aside>
⏩ Section suivante :

[2) Comprendre le TP 1](2%20Comprendre%20le%20TP%201%20cc48c771ac9a4ac3b61af7d70477ce70.md) 

</aside>