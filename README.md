# Le DevOps pour les nuls

![Untitled](Le%20DevOps%20pour%20les%20nuls%20b5394941be1240d58e36ccfd9e4df1f6/Untitled.jpeg)

[1) Quelques définitions et rappels importants](Le%20DevOps%20pour%20les%20nuls%20b5394941be1240d58e36ccfd9e4df1f6/1%20Quelques%20de%CC%81finitions%20et%20rappels%20importants%2046e3c615bda44716a5409b1f59d43cb8.md)

[2) Comprendre le TP 1](Le%20DevOps%20pour%20les%20nuls%20b5394941be1240d58e36ccfd9e4df1f6/2%20Comprendre%20le%20TP%201%20cc48c771ac9a4ac3b61af7d70477ce70.md)

La suite des TPs, peut-être un jour ?

[Lexique](Le%20DevOps%20pour%20les%20nuls%20b5394941be1240d58e36ccfd9e4df1f6/Lexique%20194a59ba05964481a9b51d768279041d.md)

[TP1](Le%20DevOps%20pour%20les%20nuls%20b5394941be1240d58e36ccfd9e4df1f6/TP1%20e4d261b085cb47a9823e543862d6255a.md)

[TP2 (incomplet)](Le%20DevOps%20pour%20les%20nuls%20b5394941be1240d58e36ccfd9e4df1f6/TP2%20incomplet%20955cea26d85f439b8f4f52a22d6f4e6a.md)

[TP3 (incomplet)](Le%20DevOps%20pour%20les%20nuls%20b5394941be1240d58e36ccfd9e4df1f6/TP3%20incomplet%20c24b212bc3454c2f93152b3be4264768.md)